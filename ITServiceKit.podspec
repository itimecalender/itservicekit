#
# Be sure to run `pod lib lint ITServiceKit.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'ITServiceKit'
  s.version          = '0.1.0'
  s.summary          = 'Models and Utilities for iTIME iOS App'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://bitbucket.org/itimecalender/itservicekit'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'GuYe' => 'kooyear@gmail.com' }
  s.source           = { :git => 'https://kooyear@bitbucket.org/itimecalender/itservicekit.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'ITServiceKit/**/*'
  
  # s.resource_bundles = {
  #   'ITServiceKit' => ['ITServiceKit/Assets/*.png']
  # }

  s.public_header_files = 'ITServiceKit/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  s.dependency 'Alamofire','~> 3.1.3'
  s.dependency 'SwiftyJSON', '~> 2.3.2'
  s.dependency 'PromiseKit', '~> 3.0.0'
  #s.dependency 'ObjectMapper', '~> 1.3'


end
