//
//  Event.swift
//  ITServiceKit
//
//  Created by GuYe on 16/7/19.
//  Copyright © 2016年 itime. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import EVReflection

public class Event:EVObject {
    
    
    var event_id:String!
    var title:String!
    
    
    var current_starts_time:Int!
    var current_ends_time:Int!
    
    
    var new_starts_time:Int!
    var new_ends_time:Int!
    
    var current_note:String!
    var new_note:String!
    
    
    
    var current_location_address:String!
    var current_location_note:String!
    
    var new_location_address:String!
    var new_location_note:String!
    
    
    var current_location_latitude:Double!
    var current_location_longitude:Double!
    
    var new_location_latitude:Double!
    var new_location_longitude:Double!
    
    
    
    
    var user_id:String!
    
    var alert_time_id:Int!
    var event_type_id:Int!
    var repeat_type_id:Int!
    var visibility_type_id:Int!
    
    var event_source_id:Int!
    var calendar_type_id:Int!
    
    var is_infinite_repeat:Bool!
    var is_deleted:Bool!
    
    var repeat_ends_time:Int?
    
    var host_event_id:String!
    var user_status_id:Int?
    
    var create_at:Int!
    var update_at:Int!
    
    var ics_sequence:Int!
    
    var attendees:[Attendee]?
    
    
    // get all events for certain user
    public class func fetchAll(user_id:String,callback:(NSError?,[Event]?) -> Void) {
        
        let url = "https://event/fetch_all/\(user_id)"
        
        let parameters: Dictionary<String,AnyObject> = [
            "user_id": user_id
        ]
        
        let headers = [
            "Content-Type": "application/json"
        ]
        
        
        Alamofire.request(.GET, url, parameters: parameters,encoding: .JSON,headers:headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    
                    var events:[Event]?
                    
                    if let json = response.result.value {
                    
                        events = [Event](json: json as? String)

                    }
                    
                    callback(nil,events)
                    
                case .Failure(let error):
                    
                    callback(error,nil)

                }
        }
        
    }
    
    
    //remove all events for certain user
    public class func removeAll(user_id:String,callback:(NSError?,Bool?) -> Void){
        
        let url = "https://event/remove_all/\(user_id)"
        
        let parameters: Dictionary<String,AnyObject> = [
            "user_id": user_id
        ]
        
        let headers = [
            "Content-Type": "application/json"
        ]
        
        
        Alamofire.request(.DELETE, url, parameters: parameters,encoding: .JSON,headers:headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    
                    callback(nil,true)
                    
                case .Failure(let error):
                    
                    callback(error,false)
                    
                }
        }
        
    }
    
    //create a new event in server
    public class func create(event:Event){
        
    }
    
    //update a event in server
    public class func update(event:Event){
        
    }
    
    //remove a event
    public class func remove(event_id:String){
        
    }
    
    //json to model
    public class func jsonToModel(json:JSON) -> Event{
        
        _ = Event()
//        event.event_id = json["event_id"].stringValue
//        event.title = json["title"].stringValue
//        
//        event.current_starts_time = json["current_starts_time"].intValue
//        event.current_ends_time = json["current_ends_time"].intValue
//        
//        event.current_note = json["current_note"].stringValue
//        event.new_note = json["new_note"].stringValue
//        
//        event.current_location_address = json["current_location_address"].stringValue
//        event.current_location_note = json["current_location_note"].stringValue
//        
//        
//        event.new_location_address = json["new_location_address"].stringValue
//        event.new_location_note = json["new_location_note"].stringValue
        
        
        
        return Event()
        
    }
    
    // json to models
    public class func jsonToModels(json:JSON)->[Event]{
        
        
        return [Event()]
        
    }
    
    
    
    
    
    
}
