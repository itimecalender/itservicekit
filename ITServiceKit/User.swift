//
//  User.swift
//  ITServiceKit
//
//  Created by GuYe on 16/7/19.
//  Copyright © 2016年 itime. All rights reserved.
//

import Foundation

public class User {
    
    
    var user_id:String!
    var password:String!
    
    
    var personal_alias:String!
    
    var binding_email:String!
    var binding_facebook_id:String?
    var binding_phone:String?
    
    var profile_photo_url:String?
    var default_event_alert_time_id:Int!
    
    var device_token:String!
    var device_id:String!
    
    var default_rating_visibility_type_id:Int!
    var default_event_visibilty_type_id:Int!
    
    var if_accept_public_event_push:Int!
    
    var average_rating_value:Int!
    
    
}